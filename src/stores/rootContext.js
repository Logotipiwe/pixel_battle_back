import React, { createContext, useContext } from 'react'
import { useLocalObservable } from 'mobx-react';
import {createRootStore} from "./rootStore";


const RootContext = createContext(null)

export const RootProvider = ({ children }) => {
  const rootStore = useLocalObservable(createRootStore)
  
  return (
    <RootContext.Provider value={rootStore}>{children}</RootContext.Provider>
  )
}
export const useRootStore = () => useContext(RootContext)