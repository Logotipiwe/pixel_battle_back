import {useRef} from "react";

export const createRootStore = () => {
  let colors = [
    "rgb(253,253,254)",
    "rgb(193,193,193)",
    "rgb(132,132,132)",
    "rgb(71,71,71)",
    "rgb(0,0,1)",
    "rgb(17,203,254)",
    "rgb(112,169,233)",
    "rgb(63,136,223)",
    "rgb(7,76,241)",
    "rgb(94,48,234)",
    "rgb(253,108,92)",
    "rgb(253,37,2)",
    "rgb(228,59,122)",
    "rgb(152,36,80)",
    "rgb(56,26,147)",
    "rgb(253,206,73)",
    "rgb(253,179,63)",
    "rgb(252,133,73)",
    "rgb(253,91,54)",
    "rgb(217,81,1)",
    "rgb(147,222,68)",
    "rgb(92,190,13)",
    "rgb(194,209,23)",
    "rgb(250,197,2)",
    "rgb(210,129,2)",
  ];
  return {
    colors,
    selectedColor: colors[0],
    size: 100,
    cells: 10,
    get cellSize() {
      return this.size / this.cells;
    },
    field: undefined,
    basePosX: 0,
    basePosY: 0,
    posX: 0,
    posY: 0,
    isDragging: false,
    isDraggingActuallyStared: false,
    get fieldStyle() {
      return {top: this.posY, left: this.posX}
    },
    onMouseDown(e) {
      this.basePosX = e.clientX - this.posX
      this.basePosY = e.clientY - this.posY
      this.isDragging = true;
    },
    fillField(context) {
      this.field = new Array(this.cells).fill('').map(row => {
        return new Array(this.cells).fill('').map(cell => {
          const margin = 0;
          const rand = Math.round(Math.random() * (255 - margin * 2) + margin);
          return {color: `rgb(${rand}, ${rand}, ${rand})`}
        });
      })
      
      this.field.forEach((row, i) => {
        row.forEach((cell, j) => {
          this.fillRect(context, j, i, cell.color)
        });
      });
    },
    fillRect(context, x, y, color) {
      context.fillStyle = color;
      context.fillRect(y * this.cellSize, x * this.cellSize, this.cellSize, this.cellSize);
    },
    onAppDidMount(ref) {
      const current = ref.current;
      const context = current.getContext('2d');
      this.fillField(context);
    },
    onCanvasClick(ref,e) {
      if (this.isDraggingActuallyStared) {
        this.isDraggingActuallyStared = false
        return;
      }
      const getCellNum = (pxs) => {
        return Math.floor(pxs / this.cellSize);
      }
      const cellIndex = getCellNum(e.clientX - this.posX);
      const rowIndex = getCellNum(e.clientY - this.posY);
      const cell = this.field[rowIndex][cellIndex];
      if (cell) {
        this.fillRect(
          ref.current.getContext('2d'),
          rowIndex, cellIndex, this.selectedColor
        );
      }
    },
    endDragging(e) {
      this.isDragging = false;
    },
    onMouseMove(e){
      if (this.isDragging) {
        this.isDraggingActuallyStared = true;
        const x = e.clientX - this.basePosX;
        const y = e.clientY - this.basePosY;
        this.posX = x < 0 ? x : 0
        this.posY = y < 0 ? y : 0
      }
    },
    selectColor(color){
      this.selectedColor = color;
    }
  }
}