import './App.scss';
import {Div} from "./Div";
import {useEffect, forwardRef, useRef} from "react";
import {useRootStore} from "./stores/rootContext";
import {observer} from "mobx-react";

let throttlingFunc;
const throttle = f => {
  clearTimeout(throttlingFunc);
  throttlingFunc = setTimeout(f, 200);
}

const App = forwardRef(() => {
  const rootStore = useRootStore();
  useEffect(() => {
    rootStore.onAppDidMount(canvasRef);
  }, []);
  
  const canvasRef = useRef(null);
  
  return (
    <Div className="App">
      <Div
        className='field'
        style={rootStore.fieldStyle}
        onMouseDown={rootStore.onMouseDown}
        onMouseUp={rootStore.endDragging}
        onMouseLeave={rootStore.endDragging}
        onMouseMove={rootStore.onMouseMove}
      >
        <canvas
          height={rootStore.size}
          width={rootStore.size}
          id="field_canvas"
          ref={canvasRef}
          onClick={rootStore.onCanvasClick.bind(null, canvasRef)}
        />
      </Div>
      <Div className={"colors"}>
        {rootStore.colors.map(color=>{
          const isSelected = color === rootStore.selectedColor;
          return <Div
            key={color}
            onClick={rootStore.selectColor.bind(null, color)}
            style={{backgroundColor: color}}
            className={isSelected ? "color selected" : "color"}
          />
        })}
      </Div>
    </Div>
  );
});

export default observer(App);
