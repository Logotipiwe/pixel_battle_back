import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import {RootProvider} from "./stores/rootContext";

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <RootProvider>
      <App/>
    </RootProvider>
  </React.StrictMode>
);

