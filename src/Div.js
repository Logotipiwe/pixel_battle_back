export function Div(p){
  return <div
    {...p}
    className={'div ' + p.className}
  >{p.children}</div>
}